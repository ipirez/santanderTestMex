import { Component, OnInit } from '@angular/core';
import { Reactive } from "../reactive";


@Component({
  selector: 'app-sibling',
  templateUrl: './sibling.component.html',
  styleUrls: ['./sibling.component.sass']
})


export class SiblingComponent implements OnInit {

  message:string;

  constructor(private data: Reactive) { }

  ngOnInit() {
    this.data.currentMessage.subscribe(message => this.message = message)
  }



}