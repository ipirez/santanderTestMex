import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent, HttpResponse, HttpErrorResponse }   from '@angular/common/http';
import { Injectable } from "@angular/core"
import { Observable, of } from "rxjs";

const url = 'https://www.fakeurl.com';


@Injectable()
export class MyHttpInterceptor implements HttpInterceptor {
  constructor() {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
          if (request.url === url) {
           var values = {
              ...request.body, 
              key: Math.trunc(<any>Math.trunc(Math.random()*1000))
            }
            console.log(values)
              console.log('Loaded from json : ' + request.url);
              return of(new HttpResponse({ status: 200, body: values }));
          }
      console.log('Loaded from http call :' + request.url);
      return next.handle(request);
  }
      
}


