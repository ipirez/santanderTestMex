export class Form {
  constructor(
    public age: number,
    public name: string,
    public lastname: string,
    public comments: string,
  ) {  }
}
