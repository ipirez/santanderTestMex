import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Form} from '../form';
import { Reactive } from "../reactive";


@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.sass']
})
export class FormComponent  {

  constructor(private httpClient: HttpClient, private data: Reactive) {}
  message: string;
  model = new Form(0, '', '','');
  text = 'contact form'
  submitted = false;
  dataBinding : string;

  onSubmit() { 
    this.httpClient.post('https://www.fakeurl.com',this.model).subscribe( res =>{
      this.dataBinding  = JSON.stringify(res)
      this.data.changeMessage(this.dataBinding)
    })
    this.submitted = true;
   }
}


